#pragma once

/**
 * Panic
 * =====
 * Panicking should be used in situations where no automatic recovery is
 * possible of where a fatal bug was detected.  Calling :c:func:`panic()` will
 * show a message on the screen and serial console and then reboot the device.
 *
 * Keep in mind that screen space is limited and thus the message should be as
 * concise as possible.
 */

/**
 * Trigger a firmware panic.
 *
 * This function will not return but instead reboot the device.  No
 * synchronization of e.g. the filesystem is done so this could potentially lead
 * to data loss.
 */
void panic(const char *format, ...)
	__attribute__((noreturn, format(printf, 1, 2)));

/**
 * Logging
 * =======
 * All logging macros take a "subsystem" and a log message.  This subsystem
 * argument should be used to uniquely identify where a message came from.
 *
 * **Example**:
 *
 * .. code-block:: cpp
 *
 *    LOG_ERR("pmic",
 *            "Failed reading battery voltage: %s (%d)",
 *            strerror(-res),
 *            res);
 */

/* Whether to enable logging at all */
#ifndef LOG_ENABLE
#define LOG_ENABLE 1
#endif

/* Whether to enable even more verbose logging */
#ifndef LOG_ENABLE_DEBUG
#define LOG_ENABLE_DEBUG 0
#endif

/* Whether to enable colorful log */
#ifndef LOG_ENABLE_COLOR
#define LOG_ENABLE_COLOR 1
#endif

#define SEV_DEBUG "D"
#define SEV_INFO  "I"
#define SEV_WARN  "W"
#define SEV_ERR   "E"
#define SEV_CRIT  "C"

#if LOG_ENABLE && !defined(__SPHINX_DOC)

int log_msg(const char *subsys, const char *format, ...)
	__attribute__((format(printf, 2, 3)));

#if LOG_ENABLE_DEBUG
#define LOG_DEBUG(subsys, format, ...)                                         \
	log_msg(subsys, SEV_DEBUG format, ##__VA_ARGS__)
#else /* LOG_ENABLE_DEBUG */
#define LOG_DEBUG(subsys, format, ...)
#endif /* LOG_ENABLE_DEBUG */

#define LOG_INFO(subsys, format, ...)                                          \
	log_msg(subsys, SEV_INFO format, ##__VA_ARGS__)
#define LOG_WARN(subsys, format, ...)                                          \
	log_msg(subsys, SEV_WARN format, ##__VA_ARGS__)
#define LOG_ERR(subsys, format, ...)                                           \
	log_msg(subsys, SEV_ERR format, ##__VA_ARGS__)
#define LOG_CRIT(subsys, format, ...)                                          \
	log_msg(subsys, SEV_CRIT format, ##__VA_ARGS__)

#else /* LOG_ENABLE */

/** */
#define LOG_INFO(subsys, format, ...) 0
/** */
#define LOG_WARN(subsys, format, ...) 0
/** */
#define LOG_ERR(subsys, format, ...) 0
/** */
#define LOG_CRIT(subsys, format, ...) 0
/** Only prints when debug logging is enabled in the meson configuration. */
#define LOG_DEBUG(subsys, format, ...) 0

inline __attribute__((format(printf, 2, 3))) int
log_msg(const char *subsys, const char *format, ...)
{
	return 0;
}

#endif /* LOG_ENABLE */
