#ifndef _WORK_QUEUE_H
#define _WORK_QUEUE_H

#define WORK_QUEUE_SIZE 20
void workqueue_init(void);
int workqueue_schedule(void (*func)(void *data), void *data);
void vWorkQueueTask(void *pvParameters);

#endif
