#include <math.h>

static const uint8_t pride_colors[6][3] = {
	{ 0xe4, 0x02, 0x02 }, { 0xff, 0x8c, 0x00 }, { 0xff, 0xed, 0x00 },
	{ 0x00, 0x80, 0x26 }, { 0x00, 0x4d, 0xff }, { 0x75, 0x06, 0x87 },
};

static void epic_frame(Ctx *epicardium_ctx, float frame)
{
	int num_colors = sizeof(pride_colors) / sizeof(pride_colors[0]);
	for (int color = 0; color < num_colors; color++) {
		ctx_rgba8_stroke(
			epicardium_ctx,
			pride_colors[color][0],
			pride_colors[color][1],
			pride_colors[color][2],
			0xff
		);
		ctx_line_width(epicardium_ctx, 10.0);
		ctx_line_cap(epicardium_ctx, CTX_CAP_ROUND);

		float width =
			expf(-pow(frame / 2.0 - 5 + color / 2.0, 2)) * 55.0 +
			5.0;

		float ypos = color * 10.0 + 40.0 - (num_colors - 1) * 5.0;

		ctx_move_to(epicardium_ctx, 80.0 - width, ypos);
		ctx_line_to(epicardium_ctx, 80.0 + width, ypos);
		ctx_stroke(epicardium_ctx);
	}

	ctx_save(epicardium_ctx);

	ctx_font_size(epicardium_ctx, 20.0f);
	ctx_text_baseline(epicardium_ctx, CTX_TEXT_BASELINE_BOTTOM);
	ctx_rgba8(epicardium_ctx, 0xff, 0xff, 0xff, 0xff);

	ctx_text_align(epicardium_ctx, CTX_TEXT_ALIGN_CENTER);
	ctx_move_to(epicardium_ctx, 80.0f, 58.0f);
	ctx_text(epicardium_ctx, "Epicardium");

	if (strcmp(CARD10_VERSION, "v1.18") != 0) {
		ctx_text_align(epicardium_ctx, CTX_TEXT_ALIGN_LEFT);
		ctx_move_to(epicardium_ctx, 2.0f, 78.0f);
		ctx_text(epicardium_ctx, CARD10_VERSION);
	} else {
		ctx_move_to(epicardium_ctx, 80.0f, 78.0f);
		ctx_text(epicardium_ctx, "Queer Quinoa");
	}

	ctx_restore(epicardium_ctx);
}
