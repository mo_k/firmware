#include "epicardium.h"
#include "os/core.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void migration_delete_app_launchers(void)
{
	int fd = epic_file_opendir("/");

	struct epic_stat entry;
	for (;;) {
		epic_file_readdir(fd, &entry);

		if (entry.type == EPICSTAT_NONE) {
			// End
			break;
		}

		const char *dot = strrchr(entry.name, '.');
		if (dot && !strcmp(dot, ".py")) {
			const char launcher[] = "# Launcher script for ";
			char launcher_buf[strlen(launcher)];

			int fd = epic_file_open(entry.name, "r");

			if (fd >= 0) {
				int n = epic_file_read(
					fd, launcher_buf, sizeof(launcher_buf)
				);
				epic_file_close(fd);

				if (n == (int)sizeof(launcher_buf) &&
				    !memcmp(launcher,
					    launcher_buf,
					    sizeof(launcher_buf))) {
					LOG_INFO(
						"migration",
						"Delete old launcher %s",
						entry.name
					);
					epic_file_unlink(entry.name);
				}
			}
		}
	}

	epic_file_close(fd);
}
