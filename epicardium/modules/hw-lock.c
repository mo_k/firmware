#include "os/core.h"
#include "modules/modules.h"
#include "os/mutex.h"

#include "FreeRTOS.h"
#include "task.h"

#include <errno.h>

static struct mutex hwlock_mutex[_HWLOCK_MAX] = { { 0 } };

void hwlock_init(void)
{
	for (int i = 0; i < _HWLOCK_MAX; i++) {
		/*
		 * TODO: mutex_create() names these all these mutexes
		 *       "&hwlock_mutex[i]" which is not helpful at all.  We
		 *       should somehow rename them to the actual hwlock names.
		 */
		mutex_create(&hwlock_mutex[i]);
	}
}

void hwlock_acquire(enum hwlock_periph p)
{
	assert(p < _HWLOCK_MAX);
	mutex_lock(&hwlock_mutex[p]);
}

int hwlock_acquire_nonblock(enum hwlock_periph p)
{
	assert(p < _HWLOCK_MAX);
	if (mutex_trylock(&hwlock_mutex[p])) {
		return 0;
	} else {
		return -EBUSY;
	}
}

void hwlock_release(enum hwlock_periph p)
{
	assert(p < _HWLOCK_MAX);
	mutex_unlock(&hwlock_mutex[p]);
}
