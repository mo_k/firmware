var group__rtc__registers =
[
    [ "Register Offsets", "group__RTC__Register__Offsets.html", "group__RTC__Register__Offsets" ],
    [ "RTC_SSEC", "group__RTC__SSEC.html", "group__RTC__SSEC" ],
    [ "RTC_TODA", "group__RTC__TODA.html", "group__RTC__TODA" ],
    [ "RTC_SSECA", "group__RTC__SSECA.html", "group__RTC__SSECA" ],
    [ "RTC_CTRL", "group__RTC__CTRL.html", "group__RTC__CTRL" ],
    [ "RTC_TRIM", "group__RTC__TRIM.html", "group__RTC__TRIM" ],
    [ "RTC_OSCCTRL", "group__RTC__OSCCTRL.html", "group__RTC__OSCCTRL" ],
    [ "mxc_rtc_regs_t", "structmxc__rtc__regs__t.html", [
      [ "sec", "structmxc__rtc__regs__t.html#a8320d62d31f9e18c6b0b0e0dac31debc", null ],
      [ "ssec", "structmxc__rtc__regs__t.html#a9450e8a3ca72114d6686bcb46de95584", null ],
      [ "toda", "structmxc__rtc__regs__t.html#ad4a4f5073aea7dcc1a623e0b7e3ca3a5", null ],
      [ "sseca", "structmxc__rtc__regs__t.html#a5b608ffa87981e34088b40bada72144e", null ],
      [ "ctrl", "structmxc__rtc__regs__t.html#af1b09ede479284ab9df670cccafe9535", null ],
      [ "trim", "structmxc__rtc__regs__t.html#a8998f97220c7d7ca47f73e0e4aa48915", null ],
      [ "oscctrl", "structmxc__rtc__regs__t.html#a10afd2f5a46148353fa978b1bce5cf6f", null ]
    ] ]
];