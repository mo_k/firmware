var group__RPU__SPIXFM =
[
    [ "MXC_F_RPU_SPIXFM_DMA0ACN_POS", "group__RPU__SPIXFM.html#gaeac4673814cd7ac943bb65ea973cb143", null ],
    [ "MXC_F_RPU_SPIXFM_DMA0ACN", "group__RPU__SPIXFM.html#ga0be60fc17e7a70afd6721300d9bba4fc", null ],
    [ "MXC_F_RPU_SPIXFM_DMA1ACN_POS", "group__RPU__SPIXFM.html#ga39ddb4d39410855ce9d70d3617a6c783", null ],
    [ "MXC_F_RPU_SPIXFM_DMA1ACN", "group__RPU__SPIXFM.html#gace8bc746e53d402bf6ec7e7144e75e23", null ],
    [ "MXC_F_RPU_SPIXFM_USBACN_POS", "group__RPU__SPIXFM.html#ga5ae6321447226a8af12d7e3789905f73", null ],
    [ "MXC_F_RPU_SPIXFM_USBACN", "group__RPU__SPIXFM.html#ga6ad8555263df5b93594ea87aba51b587", null ],
    [ "MXC_F_RPU_SPIXFM_SYS0ACN_POS", "group__RPU__SPIXFM.html#ga5056fef3a07b57f6619480438e9a5b08", null ],
    [ "MXC_F_RPU_SPIXFM_SYS0ACN", "group__RPU__SPIXFM.html#gaf562dd145c4eb0f17284c74be02b9897", null ],
    [ "MXC_F_RPU_SPIXFM_SYS1ACN_POS", "group__RPU__SPIXFM.html#ga67485271573f58ed887e6ac38a5eed9e", null ],
    [ "MXC_F_RPU_SPIXFM_SYS1ACN", "group__RPU__SPIXFM.html#ga3474481d7218a2c961880e7a2ca53820", null ],
    [ "MXC_F_RPU_SPIXFM_SDMADACN_POS", "group__RPU__SPIXFM.html#gac2b9f4545a3cb5548705b38255e75e8e", null ],
    [ "MXC_F_RPU_SPIXFM_SDMADACN", "group__RPU__SPIXFM.html#gad7e81fa2503138a43eaed0a56dec0384", null ],
    [ "MXC_F_RPU_SPIXFM_SDMAIACN_POS", "group__RPU__SPIXFM.html#ga9f4c614412a382ad29d87ba32de247e4", null ],
    [ "MXC_F_RPU_SPIXFM_SDMAIACN", "group__RPU__SPIXFM.html#ga992c71a9184c0f473bd8f00140c297a2", null ],
    [ "MXC_F_RPU_SPIXFM_CRYPTOACN_POS", "group__RPU__SPIXFM.html#ga05417c33b9865f6a615d959a1da496bd", null ],
    [ "MXC_F_RPU_SPIXFM_CRYPTOACN", "group__RPU__SPIXFM.html#ga2b8bdec18aba9c5345f48926e5900542", null ],
    [ "MXC_F_RPU_SPIXFM_SDIOACN_POS", "group__RPU__SPIXFM.html#ga92a829ec21c6769a1da90cb124eb16d7", null ],
    [ "MXC_F_RPU_SPIXFM_SDIOACN", "group__RPU__SPIXFM.html#gae04c2c134a6e369d14e8edacc0b28c8e", null ]
];