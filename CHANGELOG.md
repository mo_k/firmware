# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]


## [v1.18] - 2021-12-25 - [Queer Quinoa]
[Queer Quinoa]: https://card10.badge.events.ccc.de/release/card10-v1.18-Queer-Quinoa.zip

### For Users
- Much improved battery runtime, up to 160% more time without recharging!
- Integration of the [ctx.graphics] vector graphics renderer!  This
  means much smoother looking graphics as CTX comes with anti-aliasing!
  For now, CTX is only integrated as a backend (for the existing graphics API)
  but in future releases you will also be able to use CTX directly as well.
- As part of that, we have integrated a new font.  You can also use your own, by
  replacing `lib/ctx/fira-mono.ttf` with a font of your choice and then
  rebuilding the firmware.
- Automatically return from USB storage mode after the host (= your computer)
  ejects the device.
- Added ECG streaming over BLE.  The latest version of the Android companion
  app implements the other side of this.

### For Hackers
- Disable IRQs on core 1 during all Epicardium API calls.  This means API calls
  are now always safe to use from ISRs.
- Added an [`epic_sleep()`] API call which can be used as a hint to Epicardium
  that it can enter a deep-sleep mode.  [`epic_sleep()`] will only return once
  either the time is up or an interrupt to core 1 is pending.  Pycardium now
  uses this call for all delays by default.
- Update MicroPython to v1.17.
- With the MicroPython update, pycardium now supports f-strings!
- Firmware version can now be properly read out over BLE.
- Legacy app launcher scripts in the filesystem root are deleted on startup of
  new versions now.  Newer companion app releases also no longer generate this
  launcher script.

### Internals
- Restructured `epicardium/modules` into more sensible subdirectories.
- Switched the UART peripheral to use the HIRC8 clock.
- Converted the "personal state" implementation to use the workqueue.
- Switched the SysTick in Pycardium to use the 32kHz clock source.
- Made display backlight PWM robust against changes to the MCU's PCLK speed.
- During tickless idle, lower the core-clock (PCLK) to reduce power consumption.
- Rewrote the LCD driver.
- Put the display to sleep when the backlight is off to save power.
- Fixed a 32-bit overflow in Pycardium systick code.

[ctx.graphics]: https://ctx.graphics/
[`epic_sleep()`]: https://firmware.card10.badge.events.ccc.de/epicardium/api.html#c.epic_sleep


## [v1.17] - 2021-04-04 - [R2R Rocket]
[R2R Rocket]: https://card10.badge.events.ccc.de/release/card10-v1.17-R2R-Rocket.zip

### For Users
#### Added
- Added the Bosch BSEC library for the BME680 sensor.  See the [``bme680``
  module][bme680-docs] documentation for details.
- Added a [BLE Environmental Sensing Service][ess-docs].
- Added a [BLE HID Service][ble-hid-docs].
- Added the ability to use the pulse-oximeter as a proximity sensor (makes it
  usable as a button).  Additionally a demo-application was added which uses
  this for a push-to-talk button.
- Added a [blitting][blit-docs] function to the display module.  This finally
  allows to efficiently draw pixels!
- MicroPython BLE support!
- A PNG library for pycardium: [`png`][png-docs]
- Two more config options to tweak menu button behavior: `long_press_ms` and ``retrigger_ms``
- Option to disable low battery checks via `card10.cfg`.  This is meant for
  devices where the connection between the PMICs ADMUX and the CPU's ADC is broken
  in some way, leading to the device always reporting a low battery condition.

#### Changed
- Updated the BME680 demo app with BSEC support.
- Upgraded to MicroPython 1.14.
- Open the USB mass-storage when no apps are found.
- Improved the l0dables runtime; it now handles HardFaults and app-exits
  properly.

#### Fixed
- Fixed lockup when trying to load an ELF l0dable while ELFs are disabled.
- Fixed card10 not waking up from sleep when BLE is disabled (regression from 1.15 to 1.16).
- Fixed card10 not working with the Harmonic Board disconnected (regression from
  1.15 to 1.16).

#### Removed
- Removed the battery BLE service.

### Internals
#### Added
- A work-queue API in Epicardium to schedule work that needs to be done
  asynchronously.

#### Changed
- Upgraded to newer SDK version.
- Cleaned up the BHI160 driver a bit.
- Readjusted the flash layout, to give Epicardium more space.
- Upgraded the documentation to use Sphinx 3.

[bme680-docs]: https://firmware.card10.badge.events.ccc.de/pycardium/bme680.html
[ess-docs]: https://firmware.card10.badge.events.ccc.de/bluetooth/ess.html
[ble-hid-docs]: https://firmware.card10.badge.events.ccc.de/pycardium/ble_hid.html
[blit-docs]: https://firmware.card10.badge.events.ccc.de/pycardium/display.html#display.Display.blit
[png-docs]: https://firmware.card10.badge.events.ccc.de/pycardium/png.html


## [v1.16] - 2020-12-04 - [Pandemic Potato]
[Pandemic Potato]: https://card10.badge.events.ccc.de/release/card10-v1.16-Pandemic-Potato.zip

### Added
#### For Users
- A feature to allow setting the main app on-device.
- Added compatibility to BLE 5.0 capable phones (including iPhones).
- Added a pairing dialog in the BLE app. card10 is only visible when BLE app is
  active.
- G-Watch watch face.
- App for COVID-19 exposure notification statistics.
- Experimental app using the MAX86150 pulse-oximeter.

#### For Developers
- `leds.flash_rocket()` API for making rockets flash asynchronously.
- Basic API for the MAX86150 pulse-oximeter.
- CSPRNG as a replacement for the hardware TRNG which does not seem to produce
  good entropy the way we are using it right now.
- Option to write HCI layer log files for debugging.
- Feature in `simple_menu` which detects long button presses.
- _Stub_ `ubluetooth` module. Not yet functional!

### Changed
#### For Users
- The default watch face is now G-Watch.
- Improved BLE security by only allowing man-in-the-middle protected
  pairings and specifying minimum key lengths.
- Read time/date automatically from iOS devices.
- It is now configurable whether the left/right bottom buttons or the right
  top/bottom buttons scroll in the menu via the `right_scroll` option in
  `card10.cfg`.

#### For Developers
- Updated to a newer version of MicroPython (v1.12).
- All `u{module}` MicroPython modules are now also available as `{module}` which
  brings card10 more in line with upstream.
- Updated to SDK version 0.2.1-12
- The BLE pairing database was completely overhauled.
- Use the CSPRNG for all MicroPython randomness.
- Internal changes to the way interrupts are triggered.

### Fixed
- Made the `vibra` vibration motor API more stable.
- Fixed bug which triggered reboot loops.
- Fixed bug which made the USB serial connection unresponsive.
- Fixed bug which wrote the pairings file more periodically.
- Fixed invalid filesystem locking in BLE startup.
- Only print a single warning when a sensor stream overflows instead of spamming
  the console with errors.
- Fixed issues from reloading the configuration file.
- Fixed `pycard10.py` not properly resetting the device before loading a script.
- Allow to reopen BHI160 sensor from python.
- Fixed pycardium not exiting when triggering certain failure conditions.
- Made the config parser more robust.
- Fixed a possible lockup in the handling of the serial console.


## [v1.15] - 2020-02-02 - [Okra]
[Okra]: https://card10.badge.events.ccc.de/release/card10-v1.15-Okra.zip

### Added
- Show a fault screen on the display when epicardium panics

### Fixed
- Prevent MicroPython garbage collector from delting ISRs
- Fix race conditoin when reading/writing BLE MAC address at boot.
- Fix locking of LEDs.
- Fix bug which only allowed to have a single file open at any time.
- Put all chips into standby when going to sleep
- Misc BLE fixes


## [v1.14] - 2019-12-29 - [Nettle]
[Nettle]: https://card10.badge.events.ccc.de/release/card10-v1.14-Nettle.zip

### Added
- Scripts for profiling card10 (`tools/poor-profiler`)
- `tools/ecg2wav.py` script for displaying ECG logs in audio programs like
  Audacity.

### Changed
- Ported hardware-locks & bhi160 to new mutex API
- The menu now tries to display apps without a `metadata.json` as well, if
  possible.

### Fixed
- Fixed an unguarded i2c bus transaction which caused strange issues all
  around.
- Fixed copying large files freezing card10.
- Fixed BHI160 initialization interrupt behavior.
- Properly disable BHI160 if an error occurs during init.
- Fixed bhi160 app overflowing sensor queues.
- Fixed neopixel driver not properly writing the first pixel the first
  time.
- Fixed some l0dables crashing because the SysTick timer interrupt was not
  disabled.


## [v1.13] - 2019-12-09 - [Mushroom]
[Mushroom]: https://card10.badge.events.ccc.de/release/card10-v1.13-Mushroom.zip

### Added
- ECG plotter tool (for desktop machines) which can plot ECG logs taken with card10.
- The `input()` Python function.
- Enabled the MicroPython `framebuf` module for a Pycardium-only framebuffer
  implementation.
- Added the `utime.ticks_us()` and `utime.ticks_ms()` functions for very
  accurate timing of MicroPython code.
- Added an option to use the right buttons for scrolling and the left one for
  selecting.  This will be made configurable in a future release.
- Made timezone configurable with a new `timezone` option in `card10.cfg`.
- Added a setting-menu to the ECG App.

### Changed
- Changed default timezone to CET.
- Made a few library functions callable without any parameters so they are
  easier to use.
- Refactored the `card10.cfg` config parser.

### Fixed
- Fixed the Pycardium delay implementation in preparation for features like
  button-interrupts.  Should also be more accurate now.
- Fixed the filter which is used by the ECG app.
- Fixed the display staying off while printing the sleep-messages.
- Improved the USB-Storage mode in the menu app.
- Fixed GPIO module not properly configuring a pin if both IN and ADC are given.
- Added missing documentation for `os.mkdir()` and `os.rename()`.
- Fixed all `-Wextra` warnings, including a few bugs.  Warnings exist for a reason!

### Removed
- Removed unnecessary out-of-bounds checks in display module.  Drawing outside
  the display is now perfectly fine and the pixels will silently be ignored.


## [v1.12] - 2019-10-19 - [Leek]
[Leek]: https://card10.badge.events.ccc.de/release/card10-v1.12-Leek.zip

### Added
- **USB Storage mode**!  You can now select 'USB Storage' in the menu and
  access card10's filesystem via USB.  No more rebooting into bootloader!
- LED feedback on boot.  If your display is broken, you can still see it doing
  something now.
- `./tools/pycard10.py --set-time` to set card10's system time from your host.
- 4 new functions in `utime` modules:
  * `set_time_ms()`
  * `set_unix_time_ms()`
  * `unix_time()`
  * `unix_time_ms()`

### Changed
- Updated BLE stack
- Refactored gfx API for drawing images (internal).
- Draw partially clipped primitives in all cases (Fixes menu scrolling
  animation).
- Fatal errors are now handled in a central 'panic' module.

### Fixed
- Make BLE interrupts higher priority than anything else to hopefully increase
  stability.
- Turn off BLE encryption after closing a connection.
- Fixed mainline bootloader being broken.
- Fixed menu entries being ordered by path instead of name.
- Fixed menu crashing without a message.
- Fixed QSTR build-system.


## [v1.11] - 2019-09-24 - [Karotte]
[Karotte]: https://card10.badge.events.ccc.de/release/card10-v1.11-Karotte.zip

### Added
- **Support for sleep-mode instead of full power-off.  This means the RTC now
  retains its state!**
- For debugger users: A GDB macro `task_backtrace` which allows to view
  backtraces of tasks which are currently swapped out.  Use like
  ```text
  (gdb) task_backtrace serial_task_id
  ...
  (gdb) task_backtrace dispatcher_task_id
  ...
  (gdb) task_backtrace ble_task_id
  ```
- BHI160 magnetometer sensor
- ESB API in Pycardium.
- Monotonic clock API
- New FOSS font ...

### Changed
- `Display.print()` uses a transparent background when printing with `bg == fg`.
- Try different crc16 module during build because different environments might
  have different ones installed.
- Improved ECG app, it can now blink on pulse and more!
- Improved BHI160 and BME680 apps.

### Fixed
- Fixed a regression which made it impossible to turn off the flashlight.
- Fixed CRT for l0dables not allowing to overwrite interrupt handlers.
- Fixed ECG App not closing the sensor on `KeyboardInterrupt`.
- Fixed a bug which made the power-button unresponsive when pressed during boot
  (Interrupts were getting ignored).
- Fixed `simple_menu.Menu.exit()` not actually working.
- Added a few missing locks in `leds` module.
- Added a workaround for BHI160 axis mapping not being applied in some cases.
- Added a critical-section in BLE stack initialization to prevent weird lock-ups.
- Fixed vibra module crashing when calling `vibra.vibrate()` while already running.
- Fixed sensor-sample overflow leading to I2C bus lockup.


## [v1.10] - 2019-09-05 21:42 - [JerusalemArtichoke]
[JerusalemArtichoke]: https://card10.badge.events.ccc.de/release/card10-v1.10-JerusalemArtichoke.zip

### Added
- **ws2812**: Connect Neopixels to the wristband GPIOs and make your card10
  even more colorful!
- DigiClk is now in the default prelude!
- High-pass filter and pulse detection in default ECG app.
- Actually added `uuid` module - it was not built into the firmware before,
  by accident.
- `leds.get_rgb()`: Get the current color of an LED.
- `leds.get_rocket()`: Get the current brightness of one of the rockets.
- `micropython.mem_use()` function.
- The analog-clock can now also set the time using the buttons.

### Changed
- **Pycardium**: Switched from `long-long` to `mpz` integer representation.
  This should resolve any issues with large numbers which had popped up so far.
- Refactored BME680 sensor interface.
- Made OpenOCD scripts work with more debuggers out of the box.
- Internal changes in preparation for button-interrupts.

### Fixed
- Backlight and Vibration motor were not reset when switching apps.
- Mismatch in default settings of the *Card10 Nickname* app.
- Fixed the PMIC ADC muxer not being properly reset to neutral after a
  measurement.
- Fixed wrong timezone offset calculation in `utime.time_ms()`.
- Fixed bug where `\` characters were not parsed as path separators.
- Fixed the alignment request check in our ELF l0der.
- Fixed a buffer-overflow in the config-parser.


## [v1.9] - 2019-08-28 23:23 - [IcebergLettuce]
[IcebergLettuce]: https://card10.badge.events.ccc.de/release/card10-v1.9-IcebergLettuce.zip

### Added
- `tools/pycard10.py`: Tool to interact with card10's serial connection and
  upload files directly:
  ```bash
  ./tools/pycard10.py path/to/python-script.py
  ```
- `epic_disp_print_adv` & `Display.print(font=...)`: Print with different
  fonts!  The following fonts are supported: `8px`, `12px`, `16px`, `20px`,
  and `24px`.
- **pycardium**: Support for RAW REPL mode.
- **bhi160**: Function to disable all sensors (`bhi160.disable_all_sensors()`).
- `ls_cmsis_dap`: A tool to enumerate CMSIS-DAP debuggers.
- Tons of new features to `simple_menu`: Timeout, scrolling of long texts,
  robustness against crashes, and proper exiting.
- `card10.cfg` config file which allows enabling *ELF* files.
- Analog read for wristband GPIOs.

### Changed
- Refactored *menu* and *personal-state* apps.
- `main.py` was moved into an app to allow easier reconfiguration of the
  default app.  The new `main.py` points to the "old" one so behavior is not
  changed.
- After a timeout, the menu will close and `main.py` will run again.
- BLE security updates.
- More detailed battery state display in nickname app.
- Improved ECG app.

### Removed
- Some unused font files.

### Fixed
- Fixed a regression which made the ECG app no longer work.
- Fixed card10 advertising support for AT-commands.
- Rectangles being one pixel too small.



## [v1.8] - 2019-08-27 11:38 - [HabaneroChilli]
[HabaneroChilli]: https://card10.badge.events.ccc.de/release/card10-v1.8-HabaneroChilli.zip

### Added
- API-call for direct light-sensor readout: `epic_light_sensor_read`.
- Pause mode in ECG-App.
- `bin` field in metatdata for an alternate entrypoint.
- `shell.nix`: Nix-Shell which installs patched OpenOCD and dependencies.
- Cool LED animation in default ECG app.

### Changed
- No longer require locking the display for setting the backlight.


## [v1.7] - 2019-08-24 21:48 - [Garlic]
[Garlic]: https://card10.badge.events.ccc.de/release/card10-v1.7-Garlic.zip

### Added
- **ESB**: Epic Serial Bus (Better than USB!), stability improvements of the
  USB module.  Preparation for mass-storage access in the Firmware.
- Enabled the Hardware Watchdog;  Card10 will reset itself if the firmware crashes
- Log messages when BLE is pairing / connected.
- The name of the offending app is printed to the serial console, if an app
  crashes the metatdata parser.

### Changed
- Improved log messages in cases of lock-contention.
- Menu will show an error message if a crash occurs.

### Fixed
- Fixed race-conditions in serial writes by using a queue.
- "Card10 Nickname" crashing if only `nickname.txt` exists.
- Lockup when debug prints are enabled.
- Delayed BHI160 startup a bit so the PMIC task can check the battery first.
- Relaxed the PMIC lock-timeouts so other task can take a little more time.
- Fixed off-by-one error in `gfx_line()`.
- Fixed the API interrupts sometimes getting stuck.
- Fixed binary building on MacOS.
- Fixed race-conditions in serial console prints by introducing a queue.
- Fixed API & MAX30001 mutexes being initialized late sometimes.
- Fixed wrong stripe width in bi flag.


## [v1.6] - 2019-08-23 20:30 - [Fennel]
[Fennel]: https://card10.badge.events.ccc.de/release/card10-v1.6-Fennel.zip

- Maxim BLE SDK update

### Added
- **BLE**: Added personal state API to card10 SVC.
- **ECG**: Support for ECG + Python app
- **BLE**: Characteristic to read the time

### Changed
- Improved performance of circle-drawing algorithm.

### Fixed
- Removed a debug print in the `bhi160` module.


## [v1.5] - 2019-08-23 00:18 - [Eggppppplant]
[Eggppppplant]: https://card10.badge.events.ccc.de/release/card10-v1.5-Eggppppplant.zip

### Added
- **bootloader**: Add an error message when flashing fails.
- **display**: Option to set backlight from Python
- **utime**: Function to read time in ms from Python

### Changed
- **gpio**: Rename constants for consistency.
- **ble**: Storing pairings outside BLE stack context
- **security**: Disable ELFs by default, prevent access to some more files

### Fixed
- **gpio**: Fix field-setting in `gpio_cfg_t`.


## [v1.4] - 2019-08-22 19:43 - [DaikonRadish]
[DaikonRadish]: https://card10.badge.events.ccc.de/release/card10-v1.4-DaikonRadish.zip

### Added
- Support for the `bme680` environmental sensor.
- Support for the `bhi160` sensor fusion.
- `simple_menu` module for creating simple menus in Python.
- `power` module to access the voltage and current measurements from the PMIC.
- Support for color themes in the default clock script:
  Color themes are read from a json file, so people can customize their clock.
  Last selected theme is saved in the `clock.json` so it is persistent.

### Changed
- Refactored BLE card10 service.
- Improved BLE file-transfer (added security).
- Replaced dynamic attribute creation with static attributes.

### Fixed
- Fixed menu listing files starting with `.`.
- Fixed `utime.set_time()` applying the timezone offset in the wrong direction.
- Fixed the PMIC driver not releasing some locks properly.


## [v1.3] - 2019-08-22 00:12 - [CCCauliflower]
[CCCauliflower]: https://card10.badge.events.ccc.de/release/card10-v1.3-cccauliflower.zip

### Added
- A splashscreen in Epicardium showing the version number.
- `os.urandom()` function.

### Changed
- BLE file-transfers now create missing folders.

### Fixed
- **gfx**: Add a linebreak before character, not after.  This prevent the last
  character being cut off.
- Fixed serial task overflowing because it had a too small stack size.
- Removed confusing MAXUSB messages.


## [v1.2] - 2019-08-21 18:18 - [Broccoli]
[Broccoli]: https://card10.badge.events.ccc.de/release/card10-v1.2-broccoli.zip

```text
8e8d8614 feat(apps): Add scope to preload
e1a7684a fix(cdcacm): Disable before printing error
4c74f061 fix(utime.c): set_time should operate in local tz
e0824843 feat(pmic): Switch off if battery is low
46ef3985 feat(pmic): Add API-call to read battery voltage
79e2fb15 feat(epicardium): Periodically check the battery voltage
5da9e074 feat(pmic): Implement AMUX reading
8c59935e py: timezone workaround
c7f59d3e fix(text_reader): Convert to unix line-endings
78a7a7f4 docs: Fix underlines in ble/card10
15649293 feat(app): Add some preloads
b12e4ef9 chore(docs): Fix utime module docs
3efbab13 feat(utime.c): add python functions to set time
38f83243 chore(docs): Fix color documentation
a966e221 chore(docs): Fix python-directives with double-module
66cd10d4 docs: Document os.reset()
5fe5fe31 docs: Document pride module
338132e5 apped apps folder to search module search path
cda91555 rename Main Clock to Home
c2935c8c fixed syntax
3017591a Rename preloaded apps to make use of hatchery folder structure
842e9ad8 feat(menu.py): support scrolling for long menu entries
fbf7c8c0 fix(menu.py) Refactored menu.py based on !138
8aa8c31f feat(ble): Store bondings
5e5c7a4f fix(menu.py): Fix color-mismatch of selector background
```

## [v1.1] - 2019-08-21 03:14 - Asparagus
### Added
- Seed ``urandom`` PRNG with ``TRNG`` peripheral.
- Show linenumbers in MicroPython tracebacks.

### Fixed
- **buttons**: Acquire lock before accessing I2C.
- **rtc**: Fix RTC getting stuck because of improper initialization.
- Make lifecycle task more important than dispatcher.

## [v1.0] - 2019-08-21 00:50
Initial release.

[Unreleased]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.18...master
[v1.18]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.17...v1.18
[v1.17]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.16...v1.17
[v1.16]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.15...v1.16
[v1.15]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.14...v1.15
[v1.14]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.13...v1.14
[v1.13]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.12...v1.13
[v1.12]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.11...v1.12
[v1.11]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.10...v1.11
[v1.10]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.9...v1.10
[v1.9]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.8...v1.9
[v1.8]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.7...v1.8
[v1.7]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.6...v1.7
[v1.6]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.5...v1.6
[v1.5]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.4...v1.5
[v1.4]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.3...v1.4
[v1.3]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.2...v1.3
[v1.2]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.1...v1.2
[v1.1]: https://git.card10.badge.events.ccc.de/card10/firmware/compare/v1.0...v1.1
[v1.0]: https://git.card10.badge.events.ccc.de/card10/firmware/-/tags/v1.0
