import buttons
import color
import display
import ble_hid
import bhi160
import config

from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_hid.keycode import Keycode

from adafruit_hid.mouse import Mouse

from adafruit_hid.consumer_control_code import ConsumerControlCode
from adafruit_hid.consumer_control import ConsumerControl


import time
import os


disp = display.open()


def keyboard_demo():
    disp.clear()
    disp.print("  card10", posy=0)
    disp.print(" Keyboard", posy=20)
    disp.print("<-F13", posy=60, font=2, fg=color.BLUE)
    disp.print("Backspace->", posy=40, font=2, posx=30, fg=color.RED)
    disp.print("Type->", posy=60, posx=90, font=2, fg=color.GREEN)
    disp.update()

    k = Keyboard(ble_hid.devices)
    kl = KeyboardLayoutUS(k)

    backspace_pressed = False

    b_old = buttons.read()
    while True:
        b_new = buttons.read()
        if not b_old == b_new:
            print(b_new)
            b_old = b_new
            if b_new & buttons.TOP_RIGHT:
                # print("back")  # for debug in REPL
                if not backspace_pressed:
                    k.press(Keycode.BACKSPACE)
                    backspace_pressed = True
            else:
                if backspace_pressed:
                    k.release(Keycode.BACKSPACE)
                    backspace_pressed = False

            if b_new & buttons.BOTTOM_RIGHT:
                # use keyboard_layout for words
                kl.write("Demo with a long text to show how fast a card10 can type!")

            if b_new & buttons.BOTTOM_LEFT:
                # add shift modifier
                # k.send(Keycode.SHIFT, Keycode.F13)
                k.send(Keycode.F13)


def mouse_demo():
    disp.clear()
    disp.print("  card10", posy=0)
    disp.print("  Mouse", posy=20)
    disp.print("Left", posy=60, fg=color.BLUE)
    disp.print("Midd", posy=40, posx=100, fg=color.RED)
    disp.print("Right", posy=60, posx=80, fg=color.GREEN)
    disp.update()

    m = Mouse(ble_hid.devices)

    def send_report(samples):
        if len(samples) > 0:
            x = -int(samples[0].z)
            y = -int(samples[0].y)
            print("Reporting", x, y)

            m.move(x, y)

    def get_buttons():
        b = buttons.read()
        return (
            b & buttons.BOTTOM_LEFT,
            b & buttons.TOP_RIGHT,
            b & buttons.BOTTOM_RIGHT,
        )

    sensor = bhi160.BHI160Orientation(sample_rate=10, callback=send_report)

    b_old = buttons.read()

    left_old, middle_old, right_old = get_buttons()
    while True:
        left_new, middle_new, right_new = get_buttons()

        if left_new != left_old:
            if left_new:
                m.press(Mouse.LEFT_BUTTON)
            else:
                m.release(Mouse.LEFT_BUTTON)

        if middle_new != middle_old:
            if middle_new:
                m.press(Mouse.MIDDLE_BUTTON)
            else:
                m.release(Mouse.MIDDLE_BUTTON)

        if right_new != right_old:
            if right_new:
                m.press(Mouse.RIGHT_BUTTON)
            else:
                m.release(Mouse.RIGHT_BUTTON)

        left_old, middle_old, right_old = (left_new, middle_new, right_new)


def control_demo():
    disp.clear()
    disp.print("  card10", posy=0)
    disp.print("  Control", posy=20)
    disp.print("Play", posy=60, fg=color.BLUE)
    disp.print("Vol+", posy=40, posx=100, fg=color.RED)
    disp.print("Vol-", posy=60, posx=100, fg=color.GREEN)
    disp.update()

    cc = ConsumerControl(ble_hid.devices)

    b_old = buttons.read()
    while True:
        b_new = buttons.read()
        if not b_old == b_new:
            print(b_new)
            b_old = b_new
            if b_new == buttons.TOP_RIGHT:
                cc.send(ConsumerControlCode.VOLUME_INCREMENT)
            elif b_new == buttons.BOTTOM_RIGHT:
                cc.send(ConsumerControlCode.VOLUME_DECREMENT)
            elif b_new == buttons.BOTTOM_LEFT:
                cc.send(ConsumerControlCode.PLAY_PAUSE)


def selection_screen():
    disp.clear()
    disp.print("card10 HID", posy=0)
    disp.print("   Demo", posy=20)
    disp.print("KBD", posy=60, fg=color.BLUE)
    disp.print("Mouse", posy=40, posx=80, fg=color.RED)
    disp.print("Control", posy=60, posx=60, fg=color.GREEN)
    disp.update()

    b_old = buttons.read()
    while True:
        b_new = buttons.read()
        if not b_old == b_new:
            print(b_new)
            b_old = b_new
            if b_new == buttons.TOP_RIGHT:
                mouse_demo()
            elif b_new == buttons.BOTTOM_RIGHT:
                control_demo()
            elif b_new == buttons.BOTTOM_LEFT:
                keyboard_demo()


def set_config(enable):
    if enable:
        config.set_string("ble_hid_enable", "true")
    else:
        config.set_string("ble_hid_enable", "false")

    disp.clear()
    disp.print("resetting", posy=0, fg=[0, 255, 255])
    disp.print("to toggle", posy=20, fg=[0, 255, 255])
    disp.print("HID state", posy=40, fg=[0, 255, 255])
    disp.update()
    os.reset()


def welcome_screen(is_enabled):
    disp.clear()
    disp.print("card10 HID", posy=0)
    disp.print("   Demo", posy=20)

    if is_enabled:
        disp.print("Start ->", posy=40, posx=40, fg=color.GREEN)
        disp.print("<- Disable", posy=60, posx=0, fg=color.RED)
    else:
        disp.print("Enable ->", posy=40, posx=30, fg=color.GREEN)

    disp.update()

    b_old = buttons.read()
    while True:
        b_new = buttons.read()
        if not b_old == b_new:
            print(b_new)
            b_old = b_new
            if b_new == buttons.TOP_RIGHT:
                if is_enabled:
                    # while buttons.read(): pass
                    selection_screen()
                else:
                    set_config(True)
            elif b_new == buttons.BOTTOM_LEFT:
                if is_enabled:
                    set_config(False)


is_enabled = False
try:
    enabled = config.get_string("ble_hid_enable")
    if enabled.lower() == "true" or enabled == "1":
        is_enabled = True
except OSError:
    pass

welcome_screen(is_enabled)
