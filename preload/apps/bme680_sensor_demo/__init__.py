"""
BME680 Demo
===========
This app shows the current readings of the BME680 sensor
"""
import buttons
import color
import display
import os
import time
import bme680
import config

disp = display.open()


def rate_iaq(iaq):
    # Ratings (according to BME680 datasheet):
    #
    # Air Quality
    # Impact (long-term exposure)
    # Suggested action

    if iaq > 350:
        # "Extremely Polluted"
        # "Headaches, additional neurotoxic effects possible"
        # "Contamination needs to be identified; avoid presence in room and maximize ventilation"
        # return (color.from_hex(0x663300), "Extrem. p.")
        return (color.from_hex(0x663300), "Extremly polluted")
    if iaq > 250:
        # "Severely polluted"
        # "More severe health issue possible if harmful VOC present"
        # "Contamination should be identified if level is reached even
        # w/o presence of people; maximize ventilation & reduce attendance"
        # return (color.from_hex(0x99004c), "Severly p.")
        return (color.from_hex(0x99004C), " Severly polluted")
    if iaq > 200:
        # "Heavily polluted"
        # "Exposition might lead to effects like headache depending on type of VOCs"
        # "Optimize ventilation"
        # return (color.from_hex(0xff0000), "Heavily p.")
        return (color.from_hex(0xFF0000), " Heavily polluted")
    if iaq > 150:
        # "Moderately polluted"
        # "More significant irritation possible"
        # "Increase ventilation with clean air"
        # return (color.from_hex(0xff7e00), "Modera. p.")
        return (color.from_hex(0xFF7E00), "Moderate polluted")
    if iaq > 100:
        # "Lightly polluted"
        # "Reduction of well-being possible"
        # "Ventilation suggested"
        # return (color.from_hex(0xffff00), "Lightly p.")
        return (color.from_hex(0xFFFF00), "Lightly polluted")
    if iaq > 50:
        # "Good"
        # "No irritation or impact on well-being"
        # No Measures needed
        return (color.from_hex(0x92D050), "Good")

    # "Excellent"
    # "Pure air; best for well-being"
    # "No Measures needed"
    return (color.from_hex(0x00E400), "Excellent")


def set_config(enable):
    if enable:
        config.set_string("bsec_enable", "true")
    else:
        config.set_string("bsec_enable", "false")

    disp.clear()
    disp.print("resetting", posy=0, fg=[0, 255, 255])
    disp.print("to toggle", posy=20, fg=[0, 255, 255])
    disp.print("BSEC state", posy=40, fg=[0, 255, 255])
    disp.update()
    os.reset()


def main():
    sensor = bme680.Bme680()

    disp.clear()
    disp.print("Toggle BSEC", posy=20)
    disp.print("-->", posy=40, posx=100)
    disp.update()

    time.sleep(2)

    disp.clear().update()

    t0 = time.monotonic() - 3

    b_old = buttons.read()

    while True:
        b_new = buttons.read()
        if not b_old == b_new:
            b_old = b_new
            if b_new == buttons.TOP_RIGHT:
                enable = "false"
                try:
                    enable = config.get_string("bsec_enable")
                except OSError:
                    pass
                if enable.lower() in ["true", "1"]:
                    set_config(False)
                else:
                    set_config(True)

        if time.monotonic() - t0 >= 3:
            t0 += 3
            data = sensor.get_data()
            print(data)
            disp.clear()
            if isinstance(data, bme680.BSECData):
                disp.print("BME680 + BSEC", posx=6, font=2, fg=color.RED)
            else:
                disp.print("BME680", posx=38, font=2, fg=color.GREEN)

            disp.print("{:8.2f} C".format(data.temperature), posy=16, font=2)
            disp.print("{:8.2f} rh".format(data.humidity), posy=32, font=2)
            disp.print("{:8.2f} hPa".format(data.pressure), posy=48, font=2)

            if isinstance(data, bme680.BSECData):
                if data.iaq_accuracy == 0:
                    disp.print("IAQ: ", posy=64, font=2)
                    disp.print("Warming up", posy=64, posx=45 + 5, font=2, fg=color.RED)
                elif data.iaq_accuracy == 1:
                    disp.print("IAQ: ", posy=64, font=2)
                    disp.print(
                        "Calibrating", posy=64, posx=45 + 5, font=2, fg=color.YELLOW
                    )
                else:
                    if data.iaq_accuracy == 2:
                        c = color.YELLOW
                    else:
                        c = color.GREEN

                    disp.print("IAQ: ", posy=64, font=2, fg=c)

                    c, rating = rate_iaq(data.iaq)
                    # disp.print("{:3d}".format(data.iaq), posy=64, posx=45+5, font=2, fg=c)
                    disp.print(rating, posy=64, posx=45 + 5, font=2, fg=c)

            disp.update()

        time.sleep(0.1)


if __name__ == "__main__":
    main()
