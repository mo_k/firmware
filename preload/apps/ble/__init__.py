import os
import display
import time
import buttons
import sys_ble
import interrupt
import config

ble_events = []
is_active = False

STATE_IDLE = 1
STATE_NUMERIC_COMPARISON = 2
STATE_WAIT_FOR_COMPLETION = 3
STATE_FAIL = 4


def ble_callback(_):
    global ble_events

    while True:
        event = sys_ble.get_event()
        if event == sys_ble.EVENT_NONE:
            break
        ble_events.append(event)


def init():
    interrupt.set_callback(interrupt.BLE, ble_callback)
    interrupt.enable_callback(interrupt.BLE)
    sys_ble.set_bondable(True)


def load_mac():
    try:
        return config.get_string("ble_mac")
    except OSError:
        return None


def triangle(disp, x, y, left):
    yf = 1 if left else -1
    scale = 6
    disp.line(x - scale * yf, int(y + scale / 2), x, y)
    disp.line(x, y, x, y + scale)
    disp.line(x, y + scale, x - scale * yf, y + int(scale / 2))


def toggle():
    if is_active:
        config.set_string("ble_enable", "false")
    else:
        config.set_string("ble_enable", "true")

    disp.clear()
    disp.print("resetting", posy=0, fg=[0, 255, 255])
    disp.print("to toggle", posy=20, fg=[0, 255, 255])
    disp.print("BLE state", posy=40, fg=[0, 255, 255])
    disp.update()
    os.reset()


def headline():
    disp.print("BLE", posy=0, fg=[0, 255, 255])
    if is_active:
        try:
            dn = sys_ble.get_peer_device_name()
            disp.print("Active", posy=0, posx=50, fg=[0, 255, 0])
            disp.print(dn[:11], posy=20, posx=0, fg=[0, 255, 0])
        except OSError:
            disp.print("Ready", posy=0, posx=50, fg=[0, 255, 0])

        mac = load_mac()
        if mac is not None:
            disp.print(mac[9:], posy=60, fg=[0, 0, 255])
    else:
        disp.print("Off", posy=0, posx=50, fg=[255, 0, 0])


def selector():
    triangle(disp, 148, 46, False)
    disp.print("toggle", posx=25, posy=40, fg=[255, 255, 255])


try:
    active = config.get_string("ble_enable")
    if active.lower() == "true" or active == "1":
        is_active = True
except OSError:
    pass


init()
disp = display.open()
state = STATE_IDLE
v_old = buttons.read()

while True:
    v_new = buttons.read()
    v = ~v_old & v_new
    v_old = v_new

    ble_event = None
    if len(ble_events) > 0:
        ble_event = ble_events[0]
        ble_events = ble_events[1:]

    if state == STATE_IDLE:
        # print config screen
        disp.clear()
        headline()
        selector()
        disp.update()

        # wait for button press or ble_event
        if ble_event == sys_ble.EVENT_HANDLE_NUMERIC_COMPARISON:
            state = STATE_NUMERIC_COMPARISON
        if v & buttons.TOP_RIGHT:
            toggle()

    elif state == STATE_NUMERIC_COMPARISON:
        # print confirmation value
        compare_value = sys_ble.get_compare_value()
        disp.clear()

        try:
            dn = sys_ble.get_peer_device_name()
            disp.print(dn[:11], posy=0, fg=[0, 0, 255])
        except OSError:
            pass
        disp.print("Pair Code:", posy=20, fg=[0, 255, 255])
        disp.print("   %06d" % compare_value, posy=40, fg=[255, 255, 255])
        disp.print("Yes", posy=60, fg=[0, 255, 0])
        disp.print("No", posx=120, posy=60, fg=[255, 0, 0])

        disp.update()

        # wait for button press or ble_event
        if ble_event == sys_ble.EVENT_PAIRING_FAILED:
            state = STATE_FAIL
        if v & buttons.BOTTOM_LEFT:
            sys_ble.confirm_compare_value(True)
            disp.clear()
            disp.print("BLE Pairing", posy=0, fg=[0, 0, 255])
            disp.print("Please Wait", posy=40, fg=[255, 255, 255])
            disp.update()
            state = STATE_WAIT_FOR_COMPLETION
        elif v & (buttons.BOTTOM_RIGHT | buttons.TOP_RIGHT):
            sys_ble.confirm_compare_value(False)
            state = STATE_FAIL

    elif state == STATE_WAIT_FOR_COMPLETION:
        # Wait for pairing to complete
        if ble_event == sys_ble.EVENT_PAIRING_FAILED:
            state = STATE_FAIL
        elif ble_event == sys_ble.EVENT_PAIRING_COMPLETE:
            pairing_name = sys_ble.get_last_pairing_name().split("/")[-1].split(".")[0]
            disp.clear()
            disp.print("BLE Pairing", posy=0, fg=[0, 0, 255])
            disp.print("  Success", posy=20, fg=[0, 255, 0])
            disp.print("Name:", posy=40, fg=[255, 255, 255])
            disp.print("%11s" % pairing_name, posy=60, fg=[255, 255, 255])
            disp.update()
            time.sleep(5)
            os.exec("main.py")

    elif state == STATE_FAIL:
        # display fail screen and wait 5 seconds
        disp.clear()
        disp.print("BLE Pairing", posy=0, fg=[0, 0, 255])
        disp.print("   Fail", posy=40, fg=[255, 0, 0])
        disp.update()
        time.sleep(5)
        state = STATE_IDLE

    time.sleep(0.1)
