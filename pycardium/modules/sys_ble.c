#include "epicardium.h"

#include "py/obj.h"
#include "py/objlist.h"
#include "py/runtime.h"

#include <stdint.h>
#include <string.h>

static mp_obj_t mp_ble_confirm_compare_value(mp_obj_t confirmed_obj)
{
	bool confirmed = mp_obj_is_true(confirmed_obj);
	epic_ble_compare_response(confirmed);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(
	ble_confirm_compare_value_obj, mp_ble_confirm_compare_value
);

static mp_obj_t mp_ble_get_compare_value(void)
{
	return mp_obj_new_int(epic_ble_get_compare_value());
}
static MP_DEFINE_CONST_FUN_OBJ_0(
	ble_get_compare_value_obj, mp_ble_get_compare_value
);

static mp_obj_t mp_ble_get_last_pairing_name(void)
{
	char pairing_str[32];
	int status = epic_ble_get_last_pairing_name(
		pairing_str, sizeof(pairing_str)
	);
	if (status < 0) {
		mp_raise_OSError(-status);
	}

	mp_obj_t ret = mp_obj_new_str(pairing_str, strlen(pairing_str));
	return ret;
}
static MP_DEFINE_CONST_FUN_OBJ_0(
	ble_get_last_pairing_name_obj, mp_ble_get_last_pairing_name
);

static mp_obj_t mp_ble_get_peer_device_name(void)
{
	char device_name_str[32];
	int status = epic_ble_get_peer_device_name(
		device_name_str, sizeof(device_name_str)
	);
	if (status < 0) {
		mp_raise_OSError(-status);
	}

	mp_obj_t ret = mp_obj_new_str(device_name_str, strlen(device_name_str));
	return ret;
}
static MP_DEFINE_CONST_FUN_OBJ_0(
	ble_get_peer_device_name_obj, mp_ble_get_peer_device_name
);

static mp_obj_t mp_ble_get_scan_report(void)
{
	struct epic_scan_report scan_report;
	int ret = epic_ble_get_scan_report(&scan_report);

	if (ret < 0) {
		return mp_const_none;
	}

	mp_obj_t data = mp_obj_new_bytes(scan_report.data, scan_report.len);
	mp_obj_t rssi = MP_OBJ_NEW_SMALL_INT(scan_report.rssi);
	mp_obj_t eventType = MP_OBJ_NEW_SMALL_INT(scan_report.eventType);
	mp_obj_t addrType  = MP_OBJ_NEW_SMALL_INT(scan_report.addrType);
	mp_obj_t addr      = mp_obj_new_bytes(scan_report.addr, 6);

	mp_obj_t values_list[] = { data, rssi, eventType, addrType, addr };
	return mp_obj_new_tuple(5, values_list);
}

static MP_DEFINE_CONST_FUN_OBJ_0(
	ble_get_scan_report_obj, mp_ble_get_scan_report
);

static mp_obj_t mp_ble_get_event(void)
{
	struct epic_ble_event e;
	if (epic_ble_get_event(&e) >= 0) {
		epic_ble_free_event(&e);
		return mp_obj_new_int(e.type);
	}
	return mp_obj_new_int(BLE_EVENT_NONE);
}
static MP_DEFINE_CONST_FUN_OBJ_0(ble_get_event_obj, mp_ble_get_event);

static mp_obj_t mp_ble_set_bondable(mp_obj_t bondable_obj)
{
	bool bondable = mp_obj_is_true(bondable_obj);
	epic_ble_set_mode(bondable, false);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(ble_set_bondable_obj, mp_ble_set_bondable);

static mp_obj_t mp_ble_scan_start()
{
	epic_ble_set_mode(false, true);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(ble_scan_start_obj, mp_ble_scan_start);

static const mp_rom_map_elem_t ble_module_globals_table[] = {
	{ MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_sys_ble) },
	{ MP_ROM_QSTR(MP_QSTR_confirm_compare_value),
	  MP_ROM_PTR(&ble_confirm_compare_value_obj) },
	{ MP_ROM_QSTR(MP_QSTR_get_compare_value),
	  MP_ROM_PTR(&ble_get_compare_value_obj) },
	{ MP_ROM_QSTR(MP_QSTR_get_last_pairing_name),
	  MP_ROM_PTR(&ble_get_last_pairing_name_obj) },
	{ MP_ROM_QSTR(MP_QSTR_get_peer_device_name),
	  MP_ROM_PTR(&ble_get_peer_device_name_obj) },
	{ MP_ROM_QSTR(MP_QSTR_get_scan_report),
	  MP_ROM_PTR(&ble_get_scan_report_obj) },
	{ MP_ROM_QSTR(MP_QSTR_get_event), MP_ROM_PTR(&ble_get_event_obj) },
	{ MP_ROM_QSTR(MP_QSTR_set_bondable),
	  MP_ROM_PTR(&ble_set_bondable_obj) },
	{ MP_ROM_QSTR(MP_QSTR_scan_start), MP_ROM_PTR(&ble_scan_start_obj) },

	/* Event Numbers */
	{ MP_ROM_QSTR(MP_QSTR_EVENT_NONE),
	  MP_OBJ_NEW_SMALL_INT(BLE_EVENT_NONE) },
	{ MP_ROM_QSTR(MP_QSTR_EVENT_HANDLE_NUMERIC_COMPARISON),
	  MP_OBJ_NEW_SMALL_INT(BLE_EVENT_HANDLE_NUMERIC_COMPARISON) },
	{ MP_ROM_QSTR(MP_QSTR_EVENT_PAIRING_FAILED),
	  MP_OBJ_NEW_SMALL_INT(BLE_EVENT_PAIRING_FAILED) },
	{ MP_ROM_QSTR(MP_QSTR_EVENT_PAIRING_COMPLETE),
	  MP_OBJ_NEW_SMALL_INT(BLE_EVENT_PAIRING_COMPLETE) },
	{ MP_ROM_QSTR(MP_QSTR_EVENT_SCAN_REPORT),
	  MP_OBJ_NEW_SMALL_INT(BLE_EVENT_SCAN_REPORT) },

};
static MP_DEFINE_CONST_DICT(ble_module_globals, ble_module_globals_table);

const mp_obj_module_t ble_module = {
	.base    = { &mp_type_module },
	.globals = (mp_obj_dict_t *)&ble_module_globals,
};

/* Register the module to make it available in Python */
/* clang-format off */
MP_REGISTER_MODULE(MP_QSTR_sys_ble, ble_module, MODULE_BLE_ENABLED);
